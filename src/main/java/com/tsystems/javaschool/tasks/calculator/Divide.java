package com.tsystems.javaschool.tasks.calculator;

/**
 * Created by sofya on 17.01.2018.
 */
public class Divide extends BinaryOperation {
    public Divide(Expression left, Expression right) {
        super(left, right);
    }

    public double binaryOperation(double x, double y) throws DBZException {
        if (y == 0.) {
            throw new DBZException();
        }
        return x / y;
    }
}

