package com.tsystems.javaschool.tasks.calculator;

/**
 * Created by sofya on 17.01.2018.
 */
public class Parser {
    enum Expr {
        NUM, ADD, SUB, MUL, DIV, OPEN, CLOSE, FINISH;
    }

    private final char[] operations = {'n', '+', '-', '*', '/', '(', ')'};
    private final Expr[] parts = Expr.values();
    private Expr nowPart;
    private double value;
    private String expression;
    private int index;

    private boolean isDigit(char symbol) {
        return '0' <= symbol && symbol <= '9';
    }


    private void skipSpaces() {
        while (index < expression.length() && Character.isWhitespace(expression.charAt(index))) {
            index++;
        }
    }

    private StringBuilder parseNumber() {
        StringBuilder s = new StringBuilder();
        while (index < expression.length() && (isDigit(expression.charAt(index)) || expression.charAt(index) == '.')) {
            s.append(expression.charAt(index++));
        }
        return s;
    }

    private void parseNumberFromSB(StringBuilder s) {
        value = Double.parseDouble(s.toString());
    }

    private void nextPart() throws ParseErrorException {
        skipSpaces();
        if (index >= expression.length()) {
            nowPart = Expr.FINISH;
            return;
        }
        char symbol = expression.charAt(index);
        if (isDigit(symbol)) {
            nowPart = Expr.NUM;
            StringBuilder s = parseNumber();
            parseNumberFromSB(s);
            index--;
        } else {
            int opID = -1;
            for (int i = 0; i < operations.length; i++) {
                if (expression.charAt(index) == operations[i]) {
                    opID = i;
                    break;
                }
            }
            if (opID == -1) {
                throw new ParseErrorException();
            }
            nowPart = parts[opID];
        }
        index++;
    }

    private Expression parseSum() throws ParseErrorException {
        Expression result = parseMul();
        while (nowPart == Expr.MUL || nowPart == Expr.DIV) {
            Expr part = nowPart;
            nextPart();
            if (part == Expr.MUL) {
                result = new Multiply(result, parseMul());
            } else if (part == Expr.DIV) {
                result = new Divide(result, parseMul());
            }
        }
        return result;
    }

    private Expression parseMul() throws ParseErrorException {
        if (nowPart == Expr.NUM) {
            Double val = value;
            nextPart();
            return new Const(val);
        } else if (nowPart == Expr.OPEN) {
            nextPart();
            Expression result = parseExpression();
            if (nowPart != Expr.CLOSE) {
                throw new ParseErrorException();
            }
            nextPart();
            return result;
        } else {
            throw new ParseErrorException();
        }
    }

    private Expression parseExpression() throws ParseErrorException {
        Expression result = parseSum();
        while (nowPart == Expr.ADD || nowPart == Expr.SUB) {
            Expr part = nowPart;
            nextPart();
            if (part == Expr.ADD) {
                result = new Add(result, parseSum());
            } else {
                result = new Subtract(result, parseSum());
            }
        }
        return result;
    }

    public Expression parse(String expression) throws ParseErrorException {
        if (expression.isEmpty()) {
            throw new ParseErrorException();
        }
        this.expression = expression;
        index = 0;
        nextPart();
        Expression result = parseExpression();
        if (nowPart != Expr.FINISH) {
            throw new ParseErrorException();
        }
        return result;
    }
}

