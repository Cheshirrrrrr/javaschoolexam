package com.tsystems.javaschool.tasks.calculator;

/**
 * Created by sofya on 17.01.2018.
 */
public abstract class BinaryOperation implements Expression {
    Expression left, right;

    public BinaryOperation(Expression left, Expression right) {
        this.left = left;
        this.right = right;
    }

    protected abstract double binaryOperation(double x, double y);

    public double evaluate() {
        return binaryOperation(left.evaluate(), right.evaluate());
    }
}

