package com.tsystems.javaschool.tasks.calculator;

/**
 * Created by sofya on 17.01.2018.
 */
public class Const implements Expression {
    private double value;

    public Const(double value) {
        this.value = value;
    }

    public double evaluate() {
        return value;
    }
}
