package com.tsystems.javaschool.tasks.calculator;

import java.text.DecimalFormat;

public class Calculator {

    /**
     * Evaluate statement represented as string.
     *
     * @param statement mathematical statement containing digits, '.' (dot) as decimal mark,
     *                  parentheses, operations signs '+', '-', '*', '/'<br>
     *                  Example: <code>(1 + 38) * 4.5 - 1 / 2.</code>
     * @return string value containing result of evaluation or null if statement is invalid
     */
    public String evaluate(String statement) {
        Parser parser = new Parser();
        Expression result;
        try {
            result = parser.parse(statement);
        } catch (ParseErrorException | RuntimeException e) {
            return null;
        }
        double res;
        DecimalFormat df = new DecimalFormat("#.####");
        try {
            res = Double.valueOf(df.format(result.evaluate()));
        } catch (DBZException e) {
            return null;
        }
        if ((int)res == res) {
            return String.valueOf((int)res);
        }
        return String.valueOf(res);
    }

}
