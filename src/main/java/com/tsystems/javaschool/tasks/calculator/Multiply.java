package com.tsystems.javaschool.tasks.calculator;

/**
 * Created by sofya on 17.01.2018.
 */
public class Multiply extends BinaryOperation {
    public Multiply(Expression left, Expression right) {
        super(left, right);
    }

    public double binaryOperation(double x, double y) {
        return x * y;
    }
}
