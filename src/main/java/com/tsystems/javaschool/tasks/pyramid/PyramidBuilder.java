package com.tsystems.javaschool.tasks.pyramid;

import java.util.Collections;
import java.util.List;

public class PyramidBuilder {

    /**
     * Builds a pyramid with sorted values (with minumum value at the top line and maximum at the bottom,
     * from left to right). All vacant positions in the array are zeros.
     *
     * @param inputNumbers to be used in the pyramid
     * @return 2d array with pyramid inside
     * @throws {@link CannotBuildPyramidException} if the pyramid cannot be build with given input
     */
    public int[][] buildPyramid(List<Integer> inputNumbers) {
        int ln = inputNumbers.size();
        int n = 1;
        while (n * (n + 1L) < 2 * ln) {
            n++;
        }
        if (n * (n + 1L) / 2 > ln) {
            throw new CannotBuildPyramidException();
        }
        try {
            Collections.sort(inputNumbers);
        } catch (RuntimeException | OutOfMemoryError e) {
            throw new CannotBuildPyramidException();
        }
        int[][] res = new int[n][2 * n - 1];
        int id = 0;
        int j = n - 1;
        for (int i = 0; i < n; i++) {
            for (int k = j - i; k <= j + i; k += 2) {
                res[i][k] = inputNumbers.get(id++);
            }
        }
        return res;
    }


}
